package org.primeinternship.rushhour.repository;

import org.primeinternship.rushhour.entity.Role;
import org.primeinternship.rushhour.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
