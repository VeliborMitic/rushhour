package org.primeinternship.rushhour.repository;

import org.primeinternship.rushhour.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    @Query(value = "SELECT (COUNT(a) > 0) FROM activities a " +
            "WHERE (:myEnd > a.start_date_time AND :myBeginning < a.end_date_time)",
            nativeQuery = true)
    Boolean isOverlapping(@Param("myBeginning") Instant myBeginning,
                          @Param("myEnd") Instant myEnd);

    @Query(value = "SELECT (COUNT(a) > 0) FROM activities a " +
            "WHERE (:myEnd > a.start_date_time AND :myBeginning < a.end_date_time) AND a.activity_id <> :myId",
            nativeQuery = true)
    Boolean isOverlapping(@Param("myBeginning") Instant myBeginning,
                          @Param("myEnd") Instant myEnd,
                          @Param("myId") Long myId);
}