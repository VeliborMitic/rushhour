package org.primeinternship.rushhour.config;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.primeinternship.rushhour.model.ActivityGetModel;
import org.primeinternship.rushhour.entity.Activity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.createTypeMap(ActivityGetModel.class, Activity.class)
                .addMappings(mapping -> mapping.using(mapLongToDuration())
                        .map(ActivityGetModel::getDurationInMin, Activity::setDuration));

        modelMapper.createTypeMap(Activity.class, ActivityGetModel.class)
                .addMappings(mapping -> mapping.using(mapDurationToLong())
                        .map(Activity::getDuration, ActivityGetModel::setDurationInMin));

        return modelMapper;
    }


    private Converter<Long, Duration> mapLongToDuration() {
        return mappingContext -> {
            if (mappingContext.getSource() != null)
                return Duration.ofMinutes(mappingContext.getSource());
            return Duration.ZERO;
        };
    }

    private Converter<Duration, Long> mapDurationToLong() {
        return mappingContext -> {
            if (mappingContext.getSource() != null)
                return mappingContext.getSource().toMinutes();
            return null;
        };
    }
}