package org.primeinternship.rushhour.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
