package org.primeinternship.rushhour.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Duration;
import java.time.Instant;

@Data
@Entity
@Table(name = "activities")
public class Activity {

    @Id
    @GeneratedValue
    @Column(name = "activity_id")
    private Long id;

    private String name;

    @Column(name = "start_date_time")
    private Instant startDateTime;

    @Column(name = "end_date_time")
    private Instant endDateTime;

    private Duration duration;

    private Double price;
}
