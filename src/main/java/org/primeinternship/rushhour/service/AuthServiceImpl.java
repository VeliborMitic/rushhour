package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.model.EmailGetModel;
import org.primeinternship.rushhour.entity.Role;
import org.primeinternship.rushhour.entity.RoleName;
import org.primeinternship.rushhour.entity.User;
import org.primeinternship.rushhour.repository.RoleRepository;
import org.primeinternship.rushhour.security.JWTTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    private AuthenticationManager authenticationManager;
    private JWTTokenProvider jwtTokenProvider;
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserService userService;
    private RoleRepository roleRepository;
    private EmailService emailService;

    @Autowired
    public AuthServiceImpl(AuthenticationManager authenticationManager,
                           JWTTokenProvider jwtTokenProvider,
                           @Qualifier("JWTUserDetailsService") UserDetailsService userDetailsService,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           UserService userService,
                           RoleRepository roleRepository,
                           EmailService emailService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userService = userService;
        this.roleRepository = roleRepository;
        this.emailService = emailService;
    }

    @Override
    public Optional<String> authenticate(String email, String password) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email,
                password));
        SecurityContextHolder.getContext().setAuthentication(authenticate);

        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        if (userDetails == null) {
            return Optional.empty();
        }

        return Optional.of(jwtTokenProvider.generateToken(authenticate));
    }

    @Override
    public Optional<User> register(User user, Locale locale) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Optional<Role> optionalRole = roleRepository.findByName(RoleName.valueOf("ROLE_USER"));
        optionalRole.ifPresent(role -> user.setRoles(Collections.singleton(role)));

        userService.create(user);
        sendGreetingsEmail(user, locale);

        return Optional.of(user);
    }

    private void sendGreetingsEmail(User user, Locale locale) {
        EmailGetModel emailGetModel = new EmailGetModel();

        emailGetModel.setName(user.getFirstName() + " " + user.getLastName());
        emailGetModel.setTo(user.getEmail());
        emailGetModel.setFrom("rushhour@primeinternship.org");
        emailGetModel.setSubject("Welcome message");

        Map<String, Object> model = new HashMap<>();
        model.put("Name", emailGetModel.getName());
        model.put("username", user.getEmail());
        model.put("location", locale.getDisplayCountry());

        emailService.sendEmail(emailGetModel, model);
    }
}