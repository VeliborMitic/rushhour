package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> get(Long id);

    List<User> getAll();

    Optional<User> create(User user);

    Optional<User> update(User user);

    void delete(Long id);
}
