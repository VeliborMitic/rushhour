package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.Activity;

import java.util.List;
import java.util.Optional;

public interface ActivityService {

    Optional<Activity> get(Long id);

    List<Activity> getAll();

    Optional<Activity> create(Activity activity);

    Optional<Activity> update(Activity activity);

    void delete(Long id);
}
