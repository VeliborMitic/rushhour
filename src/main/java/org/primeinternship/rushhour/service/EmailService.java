package org.primeinternship.rushhour.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.primeinternship.rushhour.model.EmailGetModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private JavaMailSender sender;
    private Configuration config;

    @Autowired
    public EmailService(JavaMailSender sender, Configuration config) {
        this.sender = sender;
        this.config = config;
    }

    void sendEmail(EmailGetModel email, Map<String, Object> model) {
        MimeMessage message = sender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            helper.addAttachment("logo.png", new ClassPathResource("static/images/logo.png").getFile());

            Template t = config.getTemplate("email-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(email.getTo());
            helper.setText(html, true);
            helper.setSubject(email.getSubject());
            helper.setFrom(email.getFrom());
            helper.setSentDate(Date.from(Instant.now()));
            sender.send(message);

        } catch (MessagingException | IOException | TemplateException e) {
            LOGGER.error("Could not send mail", e);
        }
    }
}