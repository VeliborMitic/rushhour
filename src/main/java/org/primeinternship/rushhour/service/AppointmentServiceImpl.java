package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.Appointment;
import org.primeinternship.rushhour.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {
    private AppointmentRepository appointmentRepository;


    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    @Override
    public Optional<Appointment> get(Long id) {
        return Optional.of(appointmentRepository.getOne(id));
    }

    @Override
    public List<Appointment> getAll() {
        return appointmentRepository.findAll();
    }

    @Override
    public Optional<Appointment> create(Appointment appointment) {
        return Optional.of(appointmentRepository.save(appointment));
    }

    @Override
    public Optional<Appointment> update(Appointment appointment) {
        return Optional.of(appointmentRepository.save(appointment));
    }

    @Override
    public void delete(Long id) {
        appointmentRepository.deleteById(id);
    }
}