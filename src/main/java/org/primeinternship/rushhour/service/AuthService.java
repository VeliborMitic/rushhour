package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.User;

import java.util.Locale;
import java.util.Optional;

public interface AuthService {
    Optional<String> authenticate(String email, String password);

    Optional<User> register(User user, Locale locale);
}
