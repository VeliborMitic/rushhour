package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.security.UserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {
    @Override
    public UserPrincipal getPrincipal() {
        return getCurrentUser();
    }

    private UserPrincipal getCurrentUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null)
            return null;

        Authentication auth = context.getAuthentication();
        if (auth == null)
            return null;

        Object principal = auth.getPrincipal();
        if (!(principal instanceof UserPrincipal))
            return null;
        return (UserPrincipal) principal;
    }
}