package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.security.UserPrincipal;

public interface SecurityService {
    UserPrincipal getPrincipal();
}
