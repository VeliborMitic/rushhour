package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.Activity;
import org.primeinternship.rushhour.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {
    private ActivityRepository activityRepository;

    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public Optional<Activity> get(Long id) {
        return Optional.of(activityRepository.getOne(id));
    }

    @Override
    public List<Activity> getAll() {
        return activityRepository.findAll();
    }

    @Override
    public Optional<Activity> create(Activity activity) {
        return Optional.of(activityRepository.save(activity));
    }

    @Override
    public Optional<Activity> update(Activity activity) {
        return Optional.of(activityRepository.save(activity));
    }

    @Override
    public void delete(Long id) {
        activityRepository.deleteById(id);
    }

    public boolean isOverlapping(Instant start, Instant end) {
        return activityRepository.isOverlapping(start, end);
    }

    public boolean isOverlapping(Instant start, Instant end, Long id) {
        return activityRepository.isOverlapping(start, end, id);
    }


}
