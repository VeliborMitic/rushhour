package org.primeinternship.rushhour.service;

import org.primeinternship.rushhour.entity.Appointment;

import java.util.List;
import java.util.Optional;

public interface AppointmentService {

    Optional<Appointment> get(Long id);

    List<Appointment> getAll();

    Optional<Appointment> create(Appointment appointment);

    Optional<Appointment> update(Appointment appointment);

    void delete(Long id);
}
