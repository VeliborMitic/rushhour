package org.primeinternship.rushhour.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTTokenProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(JWTTokenProvider.class);

    @Value("${secret}")
    private String secret;
    @Value("${expiration}")
    private Long expiration;

    Long getUserIdFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        return Long.parseLong(claims.getSubject());
    }

    public String generateToken(Authentication authentication) {
        UserPrincipal authenticationPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date createdDate = new Date();
        Date expirationDate = new Date(createdDate.getTime() + expiration);

        return Jwts.builder()
                .setSubject(Long.toString(authenticationPrincipal.getId()))
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    boolean validToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            LOGGER.error("Invalid Token signature");
        } catch (MalformedJwtException ex) {
            LOGGER.error("Invalid token");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("Expired token");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("Unsupported token");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("JWT claims string is empty.");
        }
        return false;
    }
}