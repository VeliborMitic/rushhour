package org.primeinternship.rushhour.security;

import lombok.Data;

@Data
public class JsonWebToken {
    private String token;
    private String type;
}
