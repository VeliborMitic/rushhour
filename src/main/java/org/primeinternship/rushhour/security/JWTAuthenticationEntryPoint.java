package org.primeinternship.rushhour.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JWTAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException {
        LOGGER.error("Responding with error. Message - {}", e.getMessage());

        switch (httpServletRequest.getRequestURI()) {
            case "/api/v1.0/auth":
                httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong Credentials!");
                break;
            default:
                httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                        "You're not authorized to access this resource.");
                break;
        }
    }
}