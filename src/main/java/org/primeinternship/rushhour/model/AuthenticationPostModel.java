package org.primeinternship.rushhour.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class AuthenticationPostModel {
    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String password;
}
