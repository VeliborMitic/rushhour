package org.primeinternship.rushhour.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserCreatePostModel {

    @NotEmpty
    @Size(min = 3, max = 30, message = "First name can not be empty - Minimum 3, maximum 30 characters")
    private String firstName;

    @NotEmpty
    @Size(min = 3, max = 30, message = "Last name can not be empty - Minimum 3, maximum 30 characters")
    private String lastName;

    @NotEmpty
    @Email
    private String email;
}
