package org.primeinternship.rushhour.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.Instant;

@Data
public class ActivityUpdatePostModel {

    @Id
    @NotNull
    @Positive
    private Long id;

    @NotEmpty
    @Size(min = 3, max = 50, message = "Activity name can not be empty - Minimum 3, maximum 50 characters")
    private String name;

    @NotNull
    @Min(value = 30, message = "Activity duration can't be less than 30 minutes")
    @Max(value = 240, message = "Activity duration can't be more than 240 minutes")
    private Integer durationInMin;

    @NotNull
    private Double price;

    @JsonIgnore
    private Instant startDateTime;

    @JsonIgnore
    private Instant endDateTime;
}
