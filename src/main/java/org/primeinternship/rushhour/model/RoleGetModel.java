package org.primeinternship.rushhour.model;

import lombok.Data;

@Data
public class RoleGetModel {

    private Long id;

    private String role;
}
