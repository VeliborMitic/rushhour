package org.primeinternship.rushhour.model;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class AppointmentGetModel {

    private Long id;

    private List<ActivityGetModel> activities;

    private Instant startDateTime;

    private Instant endDateTime;

    private UserGetModel user;
}
