package org.primeinternship.rushhour.model;

import lombok.Data;

import java.time.Instant;

@Data
public class ActivityGetModel {

    private Long activityId;

    private String name;

    private Instant startDateTime;

    private Instant endDateTime;

    private Long durationInMin;

    private Double price;
}
