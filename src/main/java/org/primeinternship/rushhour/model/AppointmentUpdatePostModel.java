package org.primeinternship.rushhour.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.time.Instant;
import java.util.List;

@Data
public class AppointmentUpdatePostModel {

    @Id
    @NotNull
    @Positive
    private Long id;

    @NotEmpty
    @Pattern(regexp = "^\\d{4}-\\d\\d-\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?$",
            message = "Invalid date time format")
    @FutureOrPresent(message = "Invalid data! Appointment start time can not be in the past")
    private Instant startDateTime;

    @NotEmpty
    @Min(value = 1, message = "You must choose at least 1 activity")
    private List<ActivityUpdatePostModel> activities;

    @JsonIgnore
    private Instant endDateTime;
}
