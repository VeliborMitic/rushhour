package org.primeinternship.rushhour.model;

import lombok.Data;

@Data
public class EmailGetModel {
    private String name;
    private String to;
    private String from;
    private String subject;
}
