package org.primeinternship.rushhour.model;

import lombok.Data;

import java.util.Set;

@Data
public class UserGetModel {

    private Long userId;

    private String firstName;

    private String lastName;

    private String email;

    private Set<RoleGetModel> role;
}
