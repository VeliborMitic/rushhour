package org.primeinternship.rushhour.controller;

import org.modelmapper.ModelMapper;
import org.primeinternship.rushhour.entity.RoleName;
import org.primeinternship.rushhour.entity.User;
import org.primeinternship.rushhour.model.ActivityCreatePostModel;
import org.primeinternship.rushhour.model.ActivityUpdatePostModel;
import org.primeinternship.rushhour.model.AppointmentCreatePostModel;
import org.primeinternship.rushhour.model.AppointmentGetModel;
import org.primeinternship.rushhour.entity.Appointment;
import org.primeinternship.rushhour.model.AppointmentUpdatePostModel;
import org.primeinternship.rushhour.service.ActivityServiceImpl;
import org.primeinternship.rushhour.service.AppointmentServiceImpl;
import org.primeinternship.rushhour.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;

@RestController
@RequestMapping("/api/v1/appointments")
public class AppointmentController {
    private AppointmentServiceImpl appointmentService;
    private ActivityServiceImpl activityService;
    private SecurityService securityService;
    private ModelMapper modelMapper;

    @Autowired
    public AppointmentController(AppointmentServiceImpl appointmentService,
                                 ActivityServiceImpl activityService,
                                 SecurityService securityService,
                                 ModelMapper modelMapper) {
        this.appointmentService = appointmentService;
        this.activityService = activityService;
        this.securityService = securityService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable(name = "id") Long id, Locale clientLocale) {
        Optional<Appointment> optional = appointmentService.get(id);
        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        if (notInstanceOwnerOrAdmin(optional.get())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(modelMapper.map(adjustTimeToClientLocal(optional.get(), clientLocale),
                AppointmentGetModel.class));

    }

    @GetMapping("/")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity get(Locale clientLocale) {
        List<Appointment> appointments = appointmentService.getAll();

        List<AppointmentGetModel> appointmentRequestDTOList = new ArrayList<>();

        appointments.forEach(appointment -> {
            appointment.setStartDateTime(appointment.getStartDateTime().plusMillis(getUserTimeOffset(clientLocale)));
            appointment.setEndDateTime(appointment.getEndDateTime().plusMillis(getUserTimeOffset(clientLocale)));
            appointmentRequestDTOList.add(modelMapper.map(appointment, AppointmentGetModel.class));
        });

        return ResponseEntity.ok(appointmentRequestDTOList);
    }

    @PostMapping("/")
    public HttpEntity create(@Valid @RequestBody AppointmentCreatePostModel appointmentCreatePostModel, Locale clientLocale) {
        appointmentCreatePostModel.setEndDateTime(
                calculateAppointmentEndTimeCreate(appointmentCreatePostModel));

        if (isActivityOverlappingCreate(appointmentCreatePostModel))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();

        Appointment mapped = modelMapper.map(appointmentCreatePostModel, Appointment.class);

        User user = modelMapper.map(securityService.getPrincipal(), User.class);
        mapped.setUser(user);
        Optional<Appointment> optional = appointmentService.create(mapped);

        return optional.map(appointment -> ResponseEntity.ok
                (modelMapper.map(adjustTimeToClientLocal(optional.get(), clientLocale), AppointmentGetModel.class)))
                .orElseGet(() -> ResponseEntity.unprocessableEntity().build());
    }

    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable(name = "id") Long id,
                             @Valid @RequestBody AppointmentUpdatePostModel appointmentUpdatePostModel,
                             Locale clientLocale) {

        appointmentUpdatePostModel.setEndDateTime(
                calculateAppointmentEndTimeUpdate(appointmentUpdatePostModel));

        Optional<Appointment> optional = appointmentService.get(id);
        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        if (notInstanceOwnerOrAdmin(optional.get())){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        if (isActivityOverlappingUpdate(appointmentUpdatePostModel))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();

        Appointment updated = modelMapper.map(AppointmentCreatePostModel.class, Appointment.class);
        updated.setId(id);
        Optional<Appointment> optionalUpdated = appointmentService.update(updated);

        return optionalUpdated.map(appointment -> ResponseEntity.ok
                (modelMapper.map(adjustTimeToClientLocal(optionalUpdated.get(), clientLocale), AppointmentGetModel.class)))
                .orElseGet(() -> ResponseEntity.unprocessableEntity().build());
    }

    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable(name = "id") Long id) {
        Optional<Appointment> optional = appointmentService.get(id);

        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        if (notInstanceOwnerOrAdmin(optional.get())){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        appointmentService.delete(id);
        return ResponseEntity.noContent().build();

    }


    // Security check if user is updating his own appointment or is admin
    private boolean notInstanceOwnerOrAdmin(Appointment appointment){
        return  (!Objects.equals(appointment.getUser().getId(), securityService.getPrincipal().getId()) &&
                (!appointment.getUser().getRoles().contains(RoleName.ROLE_ADMIN)));
    }

    // Helper method returning true if there is activity overlapping while create Appointment
    private boolean isActivityOverlappingCreate(@RequestBody @Valid AppointmentCreatePostModel appointmentCreatePostModel) {
        Instant start = appointmentCreatePostModel.getStartDateTime();
        for (ActivityCreatePostModel activity : appointmentCreatePostModel.getActivities()){
            activity.setStartDateTime(start);
            activity.setEndDateTime(start.plusMillis(activity.getDurationInMin() * 6000));
            if(activityService.isOverlapping(
                    activity.getStartDateTime(),
                    activity.getEndDateTime())){
                return true;
            }
            start = activity.getEndDateTime();
        }
        return false;
    }

    // Helper method returning true if there is activity overlapping while updating Appointment
    private boolean isActivityOverlappingUpdate(@RequestBody @Valid AppointmentUpdatePostModel appointmentUpdatePostModel) {
        Instant start = appointmentUpdatePostModel.getStartDateTime();
        for (ActivityUpdatePostModel activity : appointmentUpdatePostModel.getActivities()){
            activity.setStartDateTime(start);
            activity.setEndDateTime(start.plusMillis(activity.getDurationInMin() * 6000));
            if(activityService.isOverlapping(
                    activity.getStartDateTime(),
                    activity.getEndDateTime(),
                    activity.getId())){
                return true;
            }
            start = activity.getEndDateTime();
        }
        return false;
    }

    private static long getUserTimeOffset(Locale clientLocale) {
        Calendar calendar = Calendar.getInstance(clientLocale);
        TimeZone timeZone = calendar.getTimeZone();
        return (long) timeZone.getRawOffset();
    }

    private static Appointment adjustTimeToClientLocal(Appointment appointment, Locale clientLocale) {
        appointment.setStartDateTime(appointment.getStartDateTime().plusMillis(getUserTimeOffset(clientLocale)));
        appointment.setEndDateTime(appointment.getEndDateTime().plusMillis(getUserTimeOffset(clientLocale)));

        return appointment;
    }

    private static Instant calculateAppointmentEndTimeCreate(AppointmentCreatePostModel appointmentCreatePostModel) {
        Instant endTime = appointmentCreatePostModel.getStartDateTime();

        for (ActivityCreatePostModel activity : appointmentCreatePostModel.getActivities()) {
            endTime = appointmentCreatePostModel.getStartDateTime().plusMillis(activity.getDurationInMin() * 60 * 1000);
        }

        return endTime;
    }

    private static Instant calculateAppointmentEndTimeUpdate(AppointmentUpdatePostModel appointmentUpdatePostModel) {
        Instant endTime = appointmentUpdatePostModel.getStartDateTime();

        for (ActivityUpdatePostModel activity : appointmentUpdatePostModel.getActivities()) {
            endTime = activity.getStartDateTime().plusMillis(activity.getDurationInMin() * 60 * 1000);
        }

        return endTime;
    }
}
