package org.primeinternship.rushhour.controller;

import org.modelmapper.ModelMapper;
import org.primeinternship.rushhour.model.AuthenticationPostModel;
import org.primeinternship.rushhour.model.RegistrationPostModel;
import org.primeinternship.rushhour.model.UserCreatePostModel;
import org.primeinternship.rushhour.entity.User;
import org.primeinternship.rushhour.security.JsonWebToken;
import org.primeinternship.rushhour.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Locale;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private AuthService authService;
    private ModelMapper modelMapper;

    @Autowired
    public AuthController(AuthService authService, ModelMapper modelMapper) {
        this.authService = authService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/signin")
    public HttpEntity authenticate(@Valid @RequestBody AuthenticationPostModel authenticationPostModel) {
        Optional<String> optionalToken = authService.authenticate(authenticationPostModel.getEmail(),
                authenticationPostModel.getPassword());

        JsonWebToken jsonWebToken = new JsonWebToken();
        jsonWebToken.setType("Bearer");

        return optionalToken.map(jwt -> {
            jsonWebToken.setToken(jwt);
            return ResponseEntity.ok(jsonWebToken);
        }).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping(value = "/register")
    public HttpEntity register(@Valid @RequestBody RegistrationPostModel registrationPostModel, Locale clientLocale) {
        User mappedUser = modelMapper.map(registrationPostModel, User.class);
        Optional<User> optionalUser = authService.register(mappedUser, clientLocale);
        return optionalUser.<HttpEntity>map(user -> ResponseEntity.ok(modelMapper.map(user, UserCreatePostModel.class)))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }
}
