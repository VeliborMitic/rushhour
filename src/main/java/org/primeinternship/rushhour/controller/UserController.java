package org.primeinternship.rushhour.controller;

import org.modelmapper.ModelMapper;
import org.primeinternship.rushhour.model.UserGetModel;
import org.primeinternship.rushhour.model.UserCreatePostModel;
import org.primeinternship.rushhour.entity.User;
import org.primeinternship.rushhour.model.UserUpdatePostModel;
import org.primeinternship.rushhour.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private UserService userService;
    private ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable(name = "id") Long id) {
        Optional<User> optional = userService.get(id);

        return optional.<HttpEntity>map(user -> ResponseEntity.ok(modelMapper.map(user, UserGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/")
    public HttpEntity get() {
        List<User> users = userService.getAll();
        List<UserGetModel> userResponseDTOList = users.stream()
                .map(user -> modelMapper.map(user, UserGetModel.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(userResponseDTOList);
    }

    @PostMapping("/")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity create(@Valid @RequestBody UserCreatePostModel userCreatePostModel) {
        User mapped = modelMapper.map(userCreatePostModel, User.class);
        Optional<User> optional = userService.create(mapped);

        return optional.<HttpEntity>map(user -> ResponseEntity.ok(modelMapper.map(user, UserGetModel.class)))
                .orElseGet(() -> ResponseEntity.unprocessableEntity().build());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity update(@PathVariable(name = "id") Long id, @Valid @RequestBody UserUpdatePostModel userUpdatePostModel) {
        User mapped = modelMapper.map(userUpdatePostModel, User.class);
        mapped.setId(id);
        Optional<User> optional = userService.update(mapped);

        return optional.<HttpEntity>map(user -> ResponseEntity.ok(modelMapper.map(user, UserGetModel.class)))
                .orElseGet(() -> ResponseEntity.unprocessableEntity().build());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity delete(@PathVariable(name = "id") Long id) {
        Optional<User> optional = userService.get(id);

        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
