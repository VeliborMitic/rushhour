package org.primeinternship.rushhour.controller;

import org.modelmapper.ModelMapper;
import org.primeinternship.rushhour.model.ActivityCreatePostModel;
import org.primeinternship.rushhour.model.ActivityGetModel;
import org.primeinternship.rushhour.model.ActivityUpdatePostModel;
import org.primeinternship.rushhour.entity.Activity;
import org.primeinternship.rushhour.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/activities")
public class ActivityController {
    private ActivityService activityService;
    private ModelMapper modelMapper;

    @Autowired
    public ActivityController(ActivityService activityService, ModelMapper modelMapper) {
        this.activityService = activityService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable(name = "id") Long id) {
        Optional<Activity> optional = activityService.get(id);

        return optional.<HttpEntity>map(activity -> ResponseEntity.ok
                (modelMapper.map(optional.get(), ActivityGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/")
    public HttpEntity get() {
        List<Activity> activities = activityService.getAll();
        List<ActivityGetModel> userResponseDTOList = activities.stream()
                .map(activity -> modelMapper.map(activity, ActivityGetModel.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(userResponseDTOList);
    }

    @PostMapping("/")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity create(@Valid @RequestBody ActivityCreatePostModel activityCreatePostModel) {
        Activity mapped = modelMapper.map(activityCreatePostModel, Activity.class);
        Optional<Activity> optional = activityService.create(mapped);

        return optional.<HttpEntity>map(activity -> ResponseEntity.ok
                (modelMapper.map(optional.get(), ActivityGetModel.class)))
                .orElseGet(() -> ResponseEntity.unprocessableEntity().build());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity update(@PathVariable(name = "id") Long id, @Valid @RequestBody ActivityUpdatePostModel activityUpdatePostModel) {
        Optional<Activity> optional = activityService.get(id);

        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Activity updated = modelMapper.map(activityUpdatePostModel, Activity.class);
        updated.setId(id);

        return ResponseEntity.ok(modelMapper.map(activityService.update(updated), ActivityGetModel.class));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpEntity delete(@PathVariable(name = "id") Long id) {
        Optional<Activity> optional = activityService.get(id);

        if (!optional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        activityService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
