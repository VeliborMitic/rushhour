<html xmlns="http://www.w3.org/1999/xhtml" lang="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Rush Hour Greetings Mail</title>
</head>

<body>
<table border="10" cellpadding="5" cellspacing="5" width="100%">
    <tr>
        <td align="center" valign="center" bgcolor="#838383"
            style="background-color: #afc6bf;"><br> <br>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="top" bgcolor="#d3be6c"
                        style="background-color: #def888; font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px; color: #000000; padding: 0px 15px 10px 15px;">

                        <div style="font-size: 30px; color:#00009b;">
                            <b>Welcome</b>
                        </div>
                        <div style="font-size: 36px; color:blue;">
                            <b>${Name} <br></b>
                        </div>
                        <div style="font-size: 30px; color:#00009b;">
                            <b>to Rush Hour API!</b>
                        </div>

                        <div style="font-size: 24px; color: #ac050a;">
                            <br> Sending Email using Spring Boot <br>with <b>FreeMarker
                                template !!!<br>
                        </div>
                        <div style="font-size: 18px; color: #00009b;">
                            <br>Rush Hour is appointment scheduling software.<br>
                            It can be used in a variety of areas, i.e. medical services,
                            beauty and wellness, sport.
                            <br>
                            <br>The application allows clients to make appointments for a given set of activities.
                        </div>
                        <div style="font-size: 14px; color: #3f0408;">
                            <br>
                            <br>Your email adress is: <b>${username}</b>
                            <br> <b>User: </b>${Name}, Location: ${location}
                            <br>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>