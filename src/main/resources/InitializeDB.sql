USE rush_hour;
INSERT INTO roles(name)
VALUES ('ROLE_USER');
INSERT INTO roles(name)
VALUES ('ROLE_ADMIN');

INSERT INTO users (user_id, email, first_name, last_name, password)
VALUES (1, 'admin@gmail.com', 'Velibor', 'Mitic', '$2a$10$EdckNEQLes1EUkuH4uwPouxtPVrbWDFKlq4g/VfVsEcSB8bIHvIli');

INSERT INTO user_role (user_id, role_id)
VALUES (1, 2);