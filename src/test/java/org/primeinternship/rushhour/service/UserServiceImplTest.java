package org.primeinternship.rushhour.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.primeinternship.rushhour.entity.Role;
import org.primeinternship.rushhour.entity.RoleName;
import org.primeinternship.rushhour.entity.User;
import org.primeinternship.rushhour.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceImplTest {

    @Mock
    private UserRepository mockUserRepository;

    private UserServiceImpl userServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        userServiceImplUnderTest = new UserServiceImpl(mockUserRepository);
    }

    @Test
    void testGet() {
        // Setup
        final Long id = 1L;
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(RoleName.ROLE_ADMIN));

        User expected = new User();
        expected.setId(id);
        expected.setFirstName("John");
        expected.setLastName("Doe");
        expected.setEmail("john.doe@johndoe.com");
        expected.setPassword("password");
        expected.setRoles(roles);
        doReturn(expected).when(mockUserRepository).getOne(id);

        // Run the test
        final Optional<User> actual = userServiceImplUnderTest.get(id);

        // Verify the results 
        actual.ifPresent(a -> {
            assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testGetAll() {
        // Setup
        User user1 = new User();
        user1.setId(1L);
        user1.setFirstName("John");
        user1.setLastName("Doe");
        user1.setEmail("john.doe@johndoe.com");
        user1.setPassword("password1");

        User user2 = new User();
        user2.setId(2L);
        user2.setFirstName("Jane");
        user2.setLastName("Doe");
        user2.setEmail("jane.doe@janedoe.com");
        user2.setPassword("password2");

        List<User> expected = new ArrayList<>();
        expected.add(user1);
        expected.add(user2);

        doReturn(expected).when(mockUserRepository).findAll();

        // Run the test
        final List<User> actual = userServiceImplUnderTest.getAll();

        // Verify the results 
        assertFalse(actual.isEmpty());
        assertEquals(expected, actual);
        assertEquals(expected.get(1), actual.get(1));
        assertEquals(2, actual.size());
    }

    @Test
    void testCreate() {
        // Setup
        final Long id = 1L;

        User expected = new User();
        expected.setId(id);
        expected.setFirstName("John");
        expected.setLastName("Doe");
        expected.setEmail("john.doe@johndoe.com");
        expected.setPassword("password");
        doReturn(expected).when(mockUserRepository).save(expected);

        // Run the test
        final Optional<User> actual = userServiceImplUnderTest.create(expected);

        // Verify the results 
        actual.ifPresent(u -> {
            assertEquals(u.getId(), expected.getId());
        });
    }

    @Test
    void testUpdate() {
        // Setup
        final Long id = 1L;

        User expected = new User();
        expected.setId(id);
        expected.setFirstName("John");
        expected.setLastName("Doe");
        expected.setEmail("john.doe@johndoe.com");
        expected.setPassword("password");
        doReturn(expected).when(mockUserRepository).save(expected);

        // Run the test
        final Optional<User> actual = userServiceImplUnderTest.update(expected);

        // Verify the results
        actual.ifPresent(u -> {
            assertEquals(u.getId(), expected.getId());
        });
    }

    @Test
    void testDelete() {
        // Setup
        final Long id = 1L;

        // Run the test
        userServiceImplUnderTest.delete(id);

        // Verify the results 
        verify(mockUserRepository).deleteById(id);
    }
}
