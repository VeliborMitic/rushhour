package org.primeinternship.rushhour.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.primeinternship.rushhour.entity.Activity;
import org.primeinternship.rushhour.repository.ActivityRepository;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ActivityServiceImplTest {

    @Mock
    private ActivityRepository mockActivityRepository;

    private ActivityServiceImpl activityServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        activityServiceImplUnderTest = new ActivityServiceImpl(mockActivityRepository);
    }

    @Test
    void testGet() {
        // Setup
        final Long id = 1L;

        Activity expected = new Activity();
        expected.setId(id);
        expected.setName("Sport");
        expected.setDuration(Duration.ZERO);
        expected.setPrice(20.0);
        doReturn(expected).when(mockActivityRepository).getOne(id);

        // Run the test
        final Optional<Activity> actual = activityServiceImplUnderTest.get(id);

        // Verify the results 
        actual.ifPresent(a -> {
            assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testGetAll() {
        // Setup
        Activity activity1 = new Activity();
        activity1.setId(1L);
        activity1.setName("a1");
        activity1.setDuration(Duration.ZERO);
        activity1.setPrice(11.0);

        Activity activity2 = new Activity();
        activity2.setId(2L);
        activity2.setName("a2");
        activity2.setDuration(Duration.ZERO);
        activity2.setPrice(12.0);

        Activity activity3 = new Activity();
        activity3.setId(3L);
        activity3.setName("a3");
        activity3.setDuration(Duration.ZERO);
        activity3.setPrice(13.0);

        final List<Activity> expected = new ArrayList<>();
        expected.add(activity1);
        expected.add(activity2);
        expected.add(activity3);

        doReturn(expected).when(mockActivityRepository).findAll();

        // Run the test
        final List<Activity> actual = activityServiceImplUnderTest.getAll();

        // Verify the results
        assertFalse(actual.isEmpty());
        assertEquals(expected, actual);
        assertEquals(expected.get(2), actual.get(2));
        assertEquals(3, actual.size());
    }

    @Test
    void testCreate() {
        // Setup
        final Long id = 1L;

        Activity expected = new Activity();
        expected.setId(id);
        expected.setName("Sport");
        expected.setDuration(Duration.ZERO);
        expected.setPrice(20.0);
        doReturn(expected).when(mockActivityRepository).save(expected);

        // Run the test
        final Optional<Activity> actual = activityServiceImplUnderTest.create(expected);

        // Verify the results 
        actual.ifPresent(a -> {
            assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testUpdate() {
        // Setup
        final Long id = 1L;

        Activity expected = new Activity();
        expected.setId(id);
        expected.setName("Sport");
        expected.setDuration(Duration.ZERO);
        expected.setPrice(20.0);
        doReturn(expected).when(mockActivityRepository).save(expected);

        // Run the test
        final Optional<Activity> actual = activityServiceImplUnderTest.update(expected);

        // Verify the results
        actual.ifPresent(a -> {
            assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testDelete() {
        // Setup
        final Long id = 1L;

        // Run the test
        activityServiceImplUnderTest.delete(id);

        // Verify the results 
        verify(mockActivityRepository).deleteById(id);
    }

    @Test
    void testIsOverlapping() {
        // Setup
        final Instant start = Instant.MIN;
        final Instant end = Instant.MAX;
        when(mockActivityRepository.isOverlapping(start, end)).thenReturn(false);

        // Run the test
        final boolean result = activityServiceImplUnderTest.isOverlapping(start, end);

        // Verify the results 
        assertFalse(result);
    }

    @Test
    void testIsOverlapping1() {
        // Setup
        final Instant start = Instant.now();
        final Instant end = Instant.now();
        final Long id = 0L;
        when(mockActivityRepository.isOverlapping(start, end, 0L)).thenReturn(true);

        // Run the test
        final boolean result = activityServiceImplUnderTest.isOverlapping(start, end, id);

        // Verify the results 
        assertTrue(result);
    }
}
