package org.primeinternship.rushhour.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.primeinternship.rushhour.entity.Appointment;
import org.primeinternship.rushhour.repository.AppointmentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class AppointmentServiceImplTest {

    @Mock
    private AppointmentRepository mockAppointmentRepository;

    private AppointmentServiceImpl appointmentServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        appointmentServiceImplUnderTest = new AppointmentServiceImpl(mockAppointmentRepository);
    }

    @Test
    void testGet() {
        // Setup
        final Long id = 1L;

        Appointment expected = new Appointment();
        expected.setId(id);
        doReturn(expected).when(mockAppointmentRepository).getOne(id);

        // Run the test
        final Optional<Appointment> actual = appointmentServiceImplUnderTest.get(1L);

        // Verify the results 
        actual.ifPresent(a -> {
            assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testGetAll() {
        // Setup
        Appointment appointment1 = new Appointment();
        appointment1.setId(1L);

        Appointment appointment2 = new Appointment();
        appointment2.setId(1L);

        List<Appointment> appointments = new ArrayList<>();
        appointments.add(appointment1);
        appointments.add(appointment2);

        doReturn(appointments).when(mockAppointmentRepository).findAll();

        // Run the test
        final List<Appointment> actual = appointmentServiceImplUnderTest.getAll();

        // Verify the results 
        assertFalse(actual.isEmpty());
        assertEquals(appointments, actual);
        assertEquals(appointments.get(1), actual.get(1));
        assertEquals(2, actual.size());
    }

    @Test
    void testCreate() {
        // Setup
        Appointment expected = new Appointment();
        expected.setId(1L);
        doReturn(expected).when(mockAppointmentRepository).save(expected);

        // Run the test
        final Optional<Appointment> actual = appointmentServiceImplUnderTest.create(expected);

        // Verify the results 
        actual.ifPresent(a -> {
            Assertions.assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testUpdate() {
        // Setup
        Appointment expected = new Appointment();
        expected.setId(1L);
        doReturn(expected).when(mockAppointmentRepository).save(expected);

        // Run the test
        final Optional<Appointment> actual = appointmentServiceImplUnderTest.update(expected);

        // Verify the results
        actual.ifPresent(a -> {
            Assertions.assertEquals(a.getId(), expected.getId());
        });
    }

    @Test
    void testDelete() {
        // Setup
        final Long id = 1L;

        // Run the test
        appointmentServiceImplUnderTest.delete(id);

        // Verify the results 
        verify(mockAppointmentRepository).deleteById(id);
    }
}
